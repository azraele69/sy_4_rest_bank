<?php


namespace App\Service;


class TransliterationService
{
  public static function CreateSlug($string)
  {
    $str = strtolower($string);
    $str = mb_convert_encoding($str, 'UTF-8', 'UTF-8');
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    $str = trim($str, "-");
    return $str;
  }

  // Переводит некоторые строки в нижний регисткр, после выборки из БД
  public static function brandLowerCase(array $items) {
    foreach ($items as &$item){
      $item['brand_name'] = mb_strtolower($item['brand_name']);
      if(isset($item['watch_collection_name'])){
        $item['watch_collection_name'] = mb_strtolower($item['watch_collection_name']);
      }
    }
    return $items;
  }
}