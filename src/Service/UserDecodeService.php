<?php


namespace App\Service;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserDecodeService extends AbstractController
{
  public function getUserNameByToken(Request $request)
  {
    $token = $request->headers->get('Authorization');
    $tokenParts = explode(".", $token);
    $tokenPayload = base64_decode($tokenParts[1]);
    $jwtPayload = json_decode($tokenPayload);
    return $jwtPayload->username;
  }

  public function verifyUserByPin (Request $request): ?User
  {
    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('login' => $this->getUserNameByToken($request)));
    if ($user->getPin() == $request->get('pin')){
      return $user;
    }
    return null;
  }

  public function CheckRole(Request $request, $role_name)
  {
    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('login' => $this->getUserNameByToken($request)));
    $role = $user->getRoles();

    return is_int(array_search($role_name, $role));
  }
}