<?php


namespace App\Service;


class ImageSaveService
{
  public static $imageDir = '/img/product/';
  private static $defaultImageFormat = '.png';

  /**
   * @param $image (base64 format)
   * @return string
   */
  public static function saveImage($image)
  {
    $imageName = self::createImageName() . self::$defaultImageFormat;
    $clearImageCode = explode(',', $image)[1];
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . self::$imageDir . $imageName , base64_decode($clearImageCode));
    return $imageName;
  }

  private static function createImageName()
  {
    return $name = time() . bin2hex(random_bytes(5));
  }
}