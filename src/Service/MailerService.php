<?php


namespace App\Service;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerService
{
  private $mail_agent = 'no-reply@eurodeniz.com';//Email с которого производиться рассылка
  private $clientManagerEmail = 'info@eurodeniz.com';//Email для манагеров по роботе с клиентами
  private $transactionManagerEmail = 'transfer@eurodeniz.com';//Email для манагеров проводящих транзакции

  private $mailer;
  public function __construct(MailerInterface $mailer)
  {
    $this->mailer = $mailer;
  }

  public function sendPinToEmail($to, $pin)
  {
    $email = (new Email())
      ->from($this->mail_agent)
      ->to($to)
      ->subject('Welcome to EURODENIZ.COM')
      ->html('<p>Your PIN: ' . $pin . '</p>');

    $this->mailer->send($email);
  }

  public function sendApplicationToTransaction(string $type)
  {
    $email = (new Email())
      ->from($this->mail_agent)

      ->to($this->transactionManagerEmail)
      ->subject('Application To Transaction')
      ->html('<p>Transaction Application: '. $type.' </p>');
    $this->mailer->send($email);
  }
}