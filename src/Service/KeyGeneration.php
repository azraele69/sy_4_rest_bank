<?php


namespace App\Service;


class KeyGeneration
{
  static public function getNewKey(): string
  {
    $date = new \DateTime();
    $bytes = random_bytes(60);
    return bin2hex($bytes) . '$' . $date->getTimestamp();

  }

  public static function getClientId()
  {
    return mt_rand(100000, 999999);
  }

  public static function getPin()
  {
    return mt_rand(1000, 9999);
  }

  public static function createAccountNumber($agent_name, $country_rating)
  {
    return mb_strtoupper(substr($agent_name, 0, 2)) . '-' . $country_rating . '-' . time();
  }

  public static function createTransferNumber ()
  {
    return mt_rand(1000, 9999) . '-' . time();
  }
}