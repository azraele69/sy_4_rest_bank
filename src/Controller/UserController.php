<?php

namespace App\Controller;

use App\Entity\Accounts;
use App\Entity\BeneficiaryTransfer;
use App\Entity\Country;
use App\Entity\User;
use App\Service\KeyGeneration;
use App\Service\UserDecodeService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


class UserController extends AbstractFOSRestController
{
  private $uds;
  private $mailer;

  public function __construct(UserDecodeService $decodeService, MailerInterface $mailer)
  {
    $this->uds = $decodeService;
    $this->mailer = $mailer;
  }

  public function sendEmail($to, $pin)
  {
    $email = (new Email())
      ->from('no-reply@eurodeniz.com')
      ->to($to)
      ->subject('Welcome to EURODENIZ.COM')
      ->html('<p>Your PIN: ' . $pin . '</p>');

    $this->mailer->send($email);
  }

  public function postSendUserPinAction(Request $request)
  {
    // получаем пользователя
    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['login' => $this->uds->getUserNameByToken($request)]);
    // генерируем новый пин
    $new_pin = KeyGeneration::getPin();
    // сохраняем новый пин в базу
    $user->setPin($new_pin);
    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();
    // отправляем пин на почту пользователя
    $this->sendEmail($user->getEmail(), $new_pin);
    return $this->view([$user->getEmail(), $new_pin], Response::HTTP_OK);
  }

  public function postVerificationAction(Request $request)
  {
    $verifyUser = $this->uds->verifyUserByPin($request);
    if (is_object($verifyUser)) {
      return $this->view(array(
        'client_id' => $verifyUser->getClientId(),
        'client_first_name' => $verifyUser->getFirstName(),
        'client_last_name' => $verifyUser->getLastName(),
        'company_name' => $verifyUser->getCompanyName(),
        'email' => $verifyUser->getEmail(),
        'accounts' => $this->getUserAccounts($verifyUser),
      ),
        Response::HTTP_OK);
    }
    return $this->view('PIN IS NOT VALID', Response::HTTP_FORBIDDEN);
  }

  public function postTokenTestAction(Request $request)
  {
    $token = $request->headers->get('Authorization');
    if (!empty($token)) {
      return $this->json(['message' => 'Сервер получил токен', 'token' => $token]);
    } else {
      return $this->json(['message' => 'Что-то не так. Я не вижу токена !!!']);
    }
  }

  private function getUserAccounts($user)
  {
    return $this->getDoctrine()->getRepository(Accounts::class)->getAccountsByUser($user);
  }

}
