<?php

namespace App\Controller;

use App\Service\UserDecodeService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


class ManagerMailerController extends AbstractFOSRestController
{
  private $mailer;
  private $senderEmail = 'no-reply@eurodeniz.com';
  private $managerEmail = 'info@eurodeniz.com';

  public function __construct(MailerInterface $mailer)
  {
    $this->mailer = $mailer;
  }

  public function postOnlineRegistrationAction(Request $request)
  {
    $email = (new Email())
      ->from($this->senderEmail)
      ->to('info@eurodeniz.com')
      ->subject('Online Registration')
      ->html($this->createRow($request));

    $this->mailer->send($email);
    unset($email);
    return $this->view($request->request->all(), Response::HTTP_OK);
  }

  public function postContactFormAction(Request $request)
  {
    $email = (new Email())
      ->from($this->senderEmail)
      ->to($this->managerEmail)
      ->subject('Contact Form')
      ->html($this->createRow($request));

    $this->mailer->send($email);
    return $this->view($request->request->all(), Response::HTTP_OK);
  }

  public function createRow(Request $request)
  {
    $fullHTML = '';
    foreach ($request->request->all() as $key => $value) {
      $fullHTML .= "<p>$key : $value </p>";
    }
    return $fullHTML;
  }

  public function postTestPushAction()
  {
    $email = (new Email())
      ->from('no-reply@eurodeniz.com')
      ->to('azraele69@gmail.com')
      ->subject('Hi this is a test')
      ->html("<p>Hello its Work ! </p>");

    $this->mailer->send($email);
    unset($email);
    return $this->view('OK', Response::HTTP_OK);
  }
}
