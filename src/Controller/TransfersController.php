<?php

namespace App\Controller;

use App\Entity\Accounts;
use App\Entity\Transfers;
use App\Entity\TransferStatus;
use App\Entity\TransferType;
use App\Entity\User;
use App\Service\KeyGeneration;
use App\Service\MailerService;
use App\Service\UserDecodeService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TransfersController extends AbstractFOSRestController
{

  private $role_name = 'CLIENT';
  private $uds;
  private $em;
  private $mailerService;

  public function __construct(UserDecodeService $decodeService, EntityManagerInterface $entityManager, MailerService $mailerService)
  {
    $this->em = $entityManager;
    $this->uds = $decodeService;
    $this->mailerService = $mailerService;
  }

  public function postInternalApplicationAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      // Проровряе принадлижат ли счета одному пользователю
      if (!$this->accountReconciliation($request)) {
        return $this->view(array('message' => 'Accounts must belong to one user'), Response::HTTP_FORBIDDEN);
      }
//      // Проверяем привышены ли лимиты на стнятие
      if ($this->WithdrawalLimit($request)) {
        return $this->view(array('message' => 'Withdrawal limit exceeded'), Response::HTTP_FORBIDDEN);
      }

      //Счёт с которого будет проводиться списание средств
      $from = $this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]);

      $transfer = new Transfers();
      $transfer->setTransferNumber(KeyGeneration::createTransferNumber());
      $transfer->setAccount($from);
      $transfer->setType($this->getDoctrine()->getRepository(TransferType::class)->find(1));
      $transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(1));
      $transfer->setDate(new \DateTime());
      $transfer->setReference($request->get('reference'));
      $transfer->setDebit($request->get('amount'));
      $transfer->setIntraToAccountNumber($request->get('to_account'));
      $transfer->setBalance($from->getBalance());

      $this->em->persist($transfer);
      $this->em->flush();

      return $this->view('OK', Response::HTTP_OK);

    } else {
      return $this->view(array('message' => 'Access closed'), Response::HTTP_FORBIDDEN);
    }
  }

  public function postPaymentRequestApplicationAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      // Проровряем принадлижит ли счет пользователю
      if (!$this->masterOfAccounts($request)) {
        return $this->view(array('message' => 'User is not an account owner'), Response::HTTP_CONFLICT);
      }
      // Проверяем привышены ли лимиты на стнятие
      if ($this->WithdrawalLimit($request)) {
        return $this->view(array('message' => 'Withdrawal limit exceeded'), Response::HTTP_CONFLICT);
      }

      //Счёт с которого будет проводиться списание средств
      $from = $this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]);

      $transfer = new Transfers();
      $transfer->setTransferNumber(KeyGeneration::createTransferNumber());
      $transfer->setAccount($from);
      $transfer->setType($this->getDoctrine()->getRepository(TransferType::class)->find(2));
      $transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(1));
      $transfer->setDate(new \DateTime());
      $transfer->setReference($request->get('Beneficiary Reference'));
      $transfer->setBalance($from->getBalance());
      $transfer->setDebit($request->get('Amount'));
      $transfer->setBankName($request->get('Bank Name'));
      $transfer->setBankAddress($request->get('Bank Address'));
      $transfer->setSwiftCode($request->get('SWIFT Code'));
      $transfer->setIbanCode($request->get('IBAN/Beneficiary Account Number'));
      $transfer->setBeneficiaryCountry($request->get('Beneficiary Country'));
      $transfer->setBeneficiaryCity($request->get('Beneficiary City'));
      $transfer->setBeneficiaryAddress($request->get('Beneficiary Address'));
      $transfer->setPayCharges($request->get('PayCharges'));

      $this->em->persist($transfer);
      $this->em->flush();
      return $this->view($request->request->all(), Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Access closed'), Response::HTTP_FORBIDDEN);
    }
  }

  public function postGetUserAccountsDetailsAction(Request $request)// получение транзакций по каждому сёту
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['login' => $this->uds->getUserNameByToken($request)]);
      $accounts = $this->getDoctrine()->getRepository(Accounts::class)->getAccountsByUser($user);

      return $this->view($this->getAccountTransferDetails($accounts), Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Access closed'), Response::HTTP_FORBIDDEN);
    }
  }

  private function getAccountTransferDetails(array $accounts)
  {
    $full_list = [];
    foreach ($accounts as $account){
      $full_list[] = [
        'account_number' => $account['account_special_number'],
        'currency' => $account['currency_abbreviation'],
        'transaction' => $this->getDoctrine()->getRepository(Transfers::class)->getTransactionByAccount($account['account_special_number'])
        ];
    }

    return $full_list;
  }

  private function accountReconciliation(Request $request)
  {
    $from = $this->getDoctrine()->getRepository(User::class)->getUserByAccount($request->get('from_account'));
    $to = $this->getDoctrine()->getRepository(User::class)->getUserByAccount($request->get('to_account'));
    return ($from[0]['id'] === $to[0]['id']) ? true : false;
  }

  private function WithdrawalLimit(Request $request)
  {
    $from = $this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]);
    return ($from->getBalance() - $from->getMinBalance()) < $request->get('amount') ? true : false;
  }

  private function masterOfAccounts(Request $request)
  {
    //Получаем пользователя сделавшего запрос на транзакцию
    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('login' => $this->uds->getUserNameByToken($request)));
    $account = $this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]);
    // получаем владельца счёта
    $account_master = $account->getUser();

    // Проверяем совпадет ли владелец и заявитель
    return $user->getId() == $account_master->getId()? true : false;
  }
}
