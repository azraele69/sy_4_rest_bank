<?php

namespace App\Controller;

use App\Entity\Accounts;
use App\Entity\AccountType;
use App\Entity\Agents;
use App\Entity\BeneficiaryTransfer;
use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\IntraTransfers;
use App\Entity\Transfers;
use App\Entity\TransferStatus;
use App\Entity\TransferType;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\KeyGeneration;
use App\Service\UserDecodeService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AdminController extends AbstractFOSRestController
{

  private $role_name = 'ROLE_MANAGER';
  private $uds;

  /**
   * @var UserRepository
   */
  private $userRepository;
  /**
   * @var UserPasswordEncoderInterface
   */
  private $passwordEncoder;
  /**
   * @var EntityManagerInterface
   */
  private $entityManager;
  private $mailer;

  public function __construct(
    UserDecodeService $decodeService,
    UserRepository $userRepository,
    UserPasswordEncoderInterface $passwordEncoder,
    EntityManagerInterface $entityManager,
    MailerInterface $mailer
  )
  {
    $this->uds = $decodeService;
    $this->userRepository = $userRepository;
    $this->passwordEncoder = $passwordEncoder;
    $this->entityManager = $entityManager;
    $this->mailer = $mailer;
  }

  public function postUserCreateAction(Request $request)
  {
//    return $this->view($request->headers, Response::HTTP_CREATED);
    if ($this->uds->CheckRole($request, $this->role_name)) {
      $user = $this->userRepository->findOneBy([
        'login' => $request->get('login'),
      ]);

      if (!is_null($user)) {
        return $this->view([
          'message' => 'User already exist'
        ], Response::HTTP_CONFLICT)->setContext((new Context())->setGroups(['public']));
      }

//      $country = $this->getDoctrine()->getRepository(Country::class)->find(2);

      $user = new User();

      $user->setLogin($request->get('login'));
      $user->setPassword($this->passwordEncoder->encodePassword($user, $request->get('password')));
      $user->setEmail($request->get('email'));
      $user->setSalutation($request->get('salutation'));
      $user->setFirstName($request->get('first_name'));
      $user->setLastName($request->get('last_name'));
      $user->setRoles(['CLIENT']);
      $user->setDateOfBirth(new \DateTime()); // $request->get('date_of_birth')
      $user->setEmail($request->get('email'));
      $user->setCompanyName($request->get('company_name'));
      $user->setCallNumber($request->get('call_number'));
      $user->setPhoneNumber($request->get('phone_number'));
      $user->setFax($request->get('fax'));
      $user->setAddress($request->get('address'));
      $user->setCity($request->get('city'));
      $user->setStateOrProvince($request->get('state_or_province'));
      $user->setUserCountry($this->getDoctrine()->getRepository(Country::class)->find($request->get('country_id')));
      $user->setClientId(KeyGeneration::getClientId());
      $user->setStatus(1);
      $user->setPin(1234);

      $this->entityManager->persist($user);
      $this->entityManager->flush();
      $user_name = $user->getFirstName() . ' ' . $user->getLastName();
      $this->sendRegistrationEmail($user->getEmail(), $user_name, $user->getLogin(), $request->get('password'));
      return $this->view($user->getId(), Response::HTTP_CREATED);
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  private function sendRegistrationEmail($to, $name, $login, $pass)
  {
    $email = (new Email())
      ->from('no-reply@eurodeniz.com')
      ->to($to)
      ->subject('Welcome to EURODENIZ.COM')
      ->html('<p>Dear ' . $name . ', </p>
            <p>Welcome to EURODENIZ.COM. Your new Eurodeniz account has been created.</p>
            <p>From now on, please log in to your account using login</p>
            ' . $login . '
            <p>And password</p>
            ' . $pass . '
            <p>If you have any questions, feel free to contact us info@eurodeniz.com </p>
            <p>Kind Regards, <br>
            EURODENIZ IBU Team <br>
            info@eurodeniz.com<br>
            Famagusta free port zone <br>
            Turkish Republic of Northern Cyprus.<br>
            +905 428 897 175</p>');
    $this->mailer->send($email);
  }

  public function postAddAccountAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
//      return $this->view($request->request->all(), Response::HTTP_OK);

      $user = $this->getDoctrine()->getRepository(User::class)->find($request->get('user_id'));
      $agent = $this->getDoctrine()->getRepository(Agents::class)->find($request->get('agent'));

      $entity = new Accounts();
      $entity->setUser($user);
      $entity->setAccountSpecialNumber(KeyGeneration::createAccountNumber($agent->getAgentName(), $user->getUserCountry()->getCountryRating()));
      $entity->setAccountType($this->getDoctrine()->getRepository(AccountType::class)->find($request->get('account_type')));
      $entity->setCurrency($this->getDoctrine()->getRepository(Currency::class)->find($request->get('currency')));
      $entity->setAgent($agent);
      $entity->setMinBalance($request->get('min_balance'));
      $entity->setBalance($request->get('balance'));

//      $entity->setDeactivationDate(null);
//      $entity->setDeactivationReason('Have no idea');
      $entity->setStatus(1);

      $em = $this->getDoctrine()->getManager();
      $em->persist($entity);
      $em->flush();

      return $this->view($entity->getId(), Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  public function postGetUserListAction()
  {
    $users_list = $this->getDoctrine()->getRepository(User::class)->getUserLoginList();
    return $this->view($users_list, Response::HTTP_OK);
  } /// TODO Сделать проверку на админа

  public function postGetUserAccountsListAction(Request $request) /// TODO Сделать проверку на админа
  {
    $user = $this->getDoctrine()->getRepository(User::class)->find($request->get('id'));
    $accounts_list = $this->getDoctrine()->getRepository(Accounts::class)->getAccountsByUser($user);
    return $this->view($accounts_list, Response::HTTP_OK);
  }

  public function postAccountOperationAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      $account = $this->getDoctrine()->getRepository(Accounts::class)->find($request->get('account')); // получаем счёт по которому будет совершаться операция
      switch ($request->get('operation')) {
        case 1:
          $this->createCreditingOperation($account, $request->get('amount'), $request->get('reference'));
          return $this->view('OK');
          break;
        case 2:
          $this->createWithdrawalOperation($account, $request->get('amount'), $request->get('reference'));
          return $this->view('OK');
          break;
        default:
          return $this->view('Transfer status cannot be changed', Response::HTTP_CONFLICT);
      }
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  private function createCreditingOperation(Accounts $account, $amount, $reference) // Функция по добавлению средст на счёт
  {
    $balance = $account->getBalance();
    $account->setBalance($balance + $amount);

    $new_transaction = new Transfers();
    $new_transaction->setAccount($account);
    $new_transaction->setType($this->getDoctrine()->getRepository(TransferType::class)->find(3));
    $new_transaction->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(3));
    $new_transaction->setDate(new \DateTime());

    $new_transaction->setCredit($amount);
    $new_transaction->setBalance($balance + $amount);
    $new_transaction->setManagerReference($reference);
    $new_transaction->setTransferNumber(KeyGeneration::createTransferNumber());

    $this->entityManager->persist($new_transaction);
    $this->entityManager->persist($account);
    $this->entityManager->flush();
  }

  private function createWithdrawalOperation(Accounts $account, $amount, $reference) // Функция по списанию средств
  {
    $balance = $account->getBalance();
    $account->setBalance($balance - $amount);

    $new_transaction = new Transfers();
    $new_transaction->setAccount($account);
    $new_transaction->setType($this->getDoctrine()->getRepository(TransferType::class)->find(4));
    $new_transaction->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(3));
    $new_transaction->setDate(new \DateTime());

    $new_transaction->setDebit($amount);
    $new_transaction->setBalance($balance - $amount);
    $new_transaction->setManagerReference($reference);
    $new_transaction->setTransferNumber(KeyGeneration::createTransferNumber());

    $this->entityManager->persist($new_transaction);
    $this->entityManager->persist($account);
    $this->entityManager->flush();
  }


  public function postGetCountriesListAction()
  {
    $list = $this->getDoctrine()->getRepository(Country::class)->getList();
    return $this->view($list, Response::HTTP_OK);
  }

  public function postGetFullParamsForAccountAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {

      return $this->view(array(
        'user' => $this->getDoctrine()->getRepository(User::class)->getUserLoginList(),
        'agent' => $this->getDoctrine()->getRepository(Agents::class)->getAgentsList(),
        'account_type' => $this->getDoctrine()->getRepository(AccountType::class)->getAccountTypeList(),
        'currency' => $this->getDoctrine()->getRepository(Currency::class)->getCurrenciesList()
      ), Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  public function postGetIntraTransfersListAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      $x = $this->getDoctrine()->getRepository(Transfers::class)->getIntraTransferList();
      $x2 = $this->addToIntraAccountInfo($x);
      return $this->view(
//        $this->getDoctrine()->getRepository(Transfers::class)->getIntraTransferList()
        $x2
        , Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  private function addToIntraAccountInfo(array $items)
  {
    foreach ($items as &$item) {
      $account = $this->getDoctrine()->getRepository(Accounts::class)->getAccountsByNumber($item['intra_to_account_number']);
      $item['to_account'] = $account['0'];
    }
    return $items;
  }

  public function postChangeIntraTransferStatusAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      $transfer = $this->getDoctrine()->getRepository(Transfers::class)->find($request->get('transfer'));
      if ($transfer->getStatus()->getId() == 1) {
        switch ($request->get('status')) {
          case 2:
            $this->setIntraTransferCanceledStatus($transfer, $request->get('reference'));
            return $this->view('CHANGED', Response::HTTP_OK);
            break;
          case 3:
            $this->setIntraTransferConfirmedStatus($transfer, $request->get('reference'), $request->get('amount'));
            return $this->view('CHANGED', Response::HTTP_OK);
            break;
          default:
            return $this->view('Transfer status cannot be changed', Response::HTTP_CONFLICT);
        }
      } else {
        return $this->view(array('message' => 'Transfer closed'), Response::HTTP_OK);
      }
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  public function postGetBeneficiaryTransfersListAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      return $this->view(
        $this->getDoctrine()->getRepository(Transfers::class)->getPaymentRequestTransferList()
        , Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  private function setIntraTransferCanceledStatus(Transfers $transfer, $reference)
  {
    //Отменено - 2
    $transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(2));
    $transfer->setManagerReference($reference);
    $this->entityManager->persist($transfer);
    $this->entityManager->flush();

  }

  private function setIntraTransferConfirmedStatus(Transfers $transfer, $reference, $amount)
  {
    $from_account = $transfer->getAccount();
    $state_from_account_balance = $from_account->getBalance();
    $from_account->setBalance($state_from_account_balance - $transfer->getDebit());

    $to_account = $this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $transfer->getIntraToAccountNumber()]);
    $to_account->setBalance($to_account->getBalance() + $amount);

    $transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(3));
    $transfer->setManagerReference($reference);
    $transfer->setBalance($state_from_account_balance - $transfer->getDebit());
    $transfer->setDate(new \DateTime());

    // Создаём новый трансфер на пополнение
    $new_transaction = new Transfers();
    $new_transaction->setAccount($to_account);
    $new_transaction->setType($this->getDoctrine()->getRepository(TransferType::class)->find(3));
    $new_transaction->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(3));
    $new_transaction->setDate(new \DateTime());
    $new_transaction->setIntraToAccountNumber($from_account->getAccountSpecialNumber());
    $new_transaction->setCredit($amount);
    $new_transaction->setBalance($to_account->getBalance());
    $new_transaction->setManagerReference($reference);
    $new_transaction->setTransferNumber(KeyGeneration::createTransferNumber());

    $this->entityManager->persist($new_transaction);
    //---------------------

    $this->entityManager->persist($transfer);
    $this->entityManager->persist($from_account);
    $this->entityManager->persist($to_account);
    $this->entityManager->flush();
  }

  public function postChangeBeneficiaryTransferStatusAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      $transfer = $this->getDoctrine()->getRepository(Transfers::class)->find($request->get('transfer'));
      if ($transfer->getStatus()->getId() == 1) {
        switch ($request->get('status')) {
          case 2:
            $this->setBeneficiaryTransferCanceledStatus($transfer, $request->get('reference'));
            return $this->view('CHANGED', Response::HTTP_OK);
            break;
          case 3:
            $this->setBeneficiaryTransferConfirmedStatus($transfer, $request->get('reference'));
            return $this->view('CHANGED', Response::HTTP_OK);
            break;
          default:
            return $this->view('Transfer status cannot be changed', Response::HTTP_CONFLICT);
        }
      } else {
        return $this->view(array('message' => 'Transfer closed'), Response::HTTP_OK);
      }
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  private function setBeneficiaryTransferCanceledStatus(Transfers $transfer, $reference)
  {
    //Отменено - 2
    $transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(2));
    $transfer->setManagerReference($reference);
    $this->entityManager->persist($transfer);
    $this->entityManager->flush();

  }

  private function setBeneficiaryTransferConfirmedStatus(Transfers $transfer, $reference)
  {
    $transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(3));
    $account = $transfer->getAccount();
    $new_balance = $account->getBalance() - $transfer->getDebit();

    $transfer->setManagerReference($reference);
    $transfer->setBalance($new_balance);
    $account->setBalance($new_balance);

    $this->entityManager->persist($transfer);
    $this->entityManager->persist($account);
    $this->entityManager->flush();
  }

  public function postGetAccountsListAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      return $this->view($this->getDoctrine()->getRepository(Accounts::class)->getAccountsList(), Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

  public function postAddBalanceAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      $account = $this->getDoctrine()->getRepository(Accounts::class)->find($request->get('id'));
      $account->setBalance($account->getBalance() + $request->get('value'));
      $this->entityManager->persist($account);
      $this->entityManager->flush();
      return $this->view($account->getId(), Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
    }
  }

//  public function postGetAccountDataAction(Request $request)
//  {
//    if ($this->uds->CheckRole($request, $this->role_name)) {
//      $account = $this->getDoctrine()->getRepository(Accounts::class)->getAccountsByNumber($request->get('num'));
//      return $this->view($account['0'], Response::HTTP_OK);
//    } else {
//      return $this->view(array('message' => 'Доступ закрыт'), Response::HTTP_FORBIDDEN);
//    }
//  }
}
