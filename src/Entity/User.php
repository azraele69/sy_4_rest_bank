<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   * @Groups({"public"})
   */
  private $id;
  /**
   * @ORM\Column(type="string", length=180)
   * @Groups({"public"})
   */
  private $email;
  /**
   * @ORM\Column(type="json", nullable=true)
   */
  private $roles = [];
  /**
   * @var string The hashed password
   * @ORM\Column(type="string")
   */
  private $password;
  
  /**
   * @ORM\Column(type="string", length=10, nullable=true)
   */
  private $salutation;

  /**
   * @ORM\Column(type="string", length=60, nullable=true)
   */
  private $first_name;

  /**
   * @ORM\Column(type="string", length=60, nullable=true)
   */
  private $last_name;

  /**
   * @ORM\Column(type="string", length=90, nullable=true)
   */
  private $company_name;

  /**
   * @ORM\Column(type="string", length=30, nullable=true)
   */
  private $call_number;

  /**
   * @ORM\Column(type="string", length=30, nullable=true)
   */
  private $phone_number;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $date_of_birth;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $client_id;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $pin;

  /**
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private $address;

  /**
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private $city;

  /**
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private $state_or_province;

  /**
   * @ORM\Column(type="boolean", nullable=true)
   */
  private $status;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $last_verification_date;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $login;

  /**
   * @ORM\Column(type="string", length=30, nullable=true)
   */
  private $fax;

  /**
   * @ORM\OneToMany(targetEntity="App\Entity\Accounts", mappedBy="user")
   */
  private $account;

  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\Country", inversedBy="users")
   */
  private $user_country;



  public function __construct()
  {
      $this->purses = new ArrayCollection();
      $this->account = new ArrayCollection();
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  public function setEmail(string $email): self
  {
    $this->email = $email;
    return $this;
  }

  /**
   * A visual identifier that represents this user.
   *
   * @see UserInterface
   */
  public function getUsername(): string
  {
    return (string)$this->login;
  }

  /**
   * @see UserInterface
   */
  public function getRoles(): array
  {
    $roles = $this->roles;
    // guarantee every user at least has ROLE_USER
    $roles[] = 'ROLE_USER';
    return array_unique($roles);
  }

  public function setRoles(array $roles): self
  {
    $this->roles = $roles;
    return $this;
  }

  /**
   * @see UserInterface
   */
  public function getPassword(): string
  {
    return (string)$this->password;
  }

  public function setPassword(string $password): self
  {
    $this->password = $password;
    return $this;
  }

  /**
   * @see UserInterface
   */
  public function getSalt()
  {
    // not needed when using the "bcrypt" algorithm in security.yaml
  }

  /**
   * @see UserInterface
   */
  public function eraseCredentials()
  {
    // If you store any temporary, sensitive data on the user, clear it here
    // $this->plainPassword = null;
  }
  
  public function getSalutation(): ?string
  {
      return $this->salutation;
  }

  public function setSalutation(string $salutation): self
  {
      $this->salutation = $salutation;

      return $this;
  }

  public function getFirstName(): ?string
  {
      return $this->first_name;
  }

  public function setFirstName(string $first_name): self
  {
      $this->first_name = $first_name;

      return $this;
  }

  public function getLastName(): ?string
  {
      return $this->last_name;
  }

  public function setLastName(string $last_name): self
  {
      $this->last_name = $last_name;

      return $this;
  }

  public function getCompanyName(): ?string
  {
      return $this->company_name;
  }

  public function setCompanyName(string $company_name): self
  {
      $this->company_name = $company_name;

      return $this;
  }

  public function getCallNumber(): ?string
  {
      return $this->call_number;
  }

  public function setCallNumber(string $call_number): self
  {
      $this->call_number = $call_number;

      return $this;
  }

  public function getPhoneNumber(): ?string
  {
      return $this->phone_number;
  }

  public function setPhoneNumber(?string $phone_number): self
  {
      $this->phone_number = $phone_number;

      return $this;
  }

  public function getDateOfBirth(): ?\DateTimeInterface
  {
      return $this->date_of_birth;
  }

  public function setDateOfBirth(?\DateTimeInterface $date_of_birth): self
  {
      $this->date_of_birth = $date_of_birth;

      return $this;
  }

  public function getClientId(): ?int
  {
      return $this->client_id;
  }

  public function setClientId(int $client_id): self
  {
      $this->client_id = $client_id;

      return $this;
  }

  public function getPin(): ?int
  {
      return $this->pin;
  }

  public function setPin(int $pin): self
  {
      $this->pin = $pin;

      return $this;
  }

  public function getAddress(): ?string
  {
      return $this->address;
  }

  public function setAddress(string $address): self
  {
      $this->address = $address;

      return $this;
  }

  public function getCity(): ?string
  {
      return $this->city;
  }

  public function setCity(string $city): self
  {
      $this->city = $city;

      return $this;
  }

  public function getStateOrProvince(): ?string
  {
      return $this->state_or_province;
  }

  public function setStateOrProvince(string $state_or_province): self
  {
      $this->state_or_province = $state_or_province;

      return $this;
  }

  public function getStatus(): ?bool
  {
      return $this->status;
  }

  public function setStatus(bool $status): self
  {
      $this->status = $status;

      return $this;
  }

  public function getLastVerificationDate(): ?\DateTimeInterface
  {
      return $this->last_verification_date;
  }

  public function setLastVerificationDate(?\DateTimeInterface $last_verification_date): self
  {
      $this->last_verification_date = $last_verification_date;

      return $this;
  }

  public function getLogin(): ?string
  {
      return $this->login;
  }

  public function setLogin(string $login): self
  {
      $this->login = $login;

      return $this;
  }

  public function getFax(): ?string
  {
      return $this->fax;
  }

  public function setFax(string $fax): self
  {
      $this->fax = $fax;

      return $this;
  }

  /**
   * @return Collection|Accounts[]
   */
  public function getAccount(): Collection
  {
      return $this->account;
  }

  public function addAccount(Accounts $account): self
  {
      if (!$this->account->contains($account)) {
          $this->account[] = $account;
          $account->setUser($this);
      }

      return $this;
  }

  public function removeAccount(Accounts $account): self
  {
      if ($this->account->contains($account)) {
          $this->account->removeElement($account);
          // set the owning side to null (unless already changed)
          if ($account->getUser() === $this) {
              $account->setUser(null);
          }
      }

      return $this;
  }

  public function getUserCountry(): ?Country
  {
      return $this->user_country;
  }

  public function setUserCountry(?Country $user_country): self
  {
      $this->user_country = $user_country;

      return $this;
  }

}
