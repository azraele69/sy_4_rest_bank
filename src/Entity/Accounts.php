<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountsRepository")
 */
class Accounts
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="account")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $account_special_number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AccountType", inversedBy="accounts")
     */
    private $account_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", inversedBy="accounts")
     */
    private $currency;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agents", inversedBy="accounts")
     */
    private $agent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $balance;

    /**
     * @ORM\Column(type="integer")
     */
    private $min_balance;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transfers", mappedBy="account")
     */
    private $transfers;



    public function __construct()
    {
        $this->transfers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAccountSpecialNumber(): ?string
    {
        return $this->account_special_number;
    }

    public function setAccountSpecialNumber(string $account_special_number): self
    {
        $this->account_special_number = $account_special_number;

        return $this;
    }

    public function getAccountType(): ?AccountType
    {
        return $this->account_type;
    }

    public function setAccountType(?AccountType $account_type): self
    {
        $this->account_type = $account_type;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAgent(): ?Agents
    {
        return $this->agent;
    }

    public function setAgent(?Agents $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getBalance(): ?int
    {
        return $this->balance;
    }

    public function setBalance(int $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getMinBalance(): ?int
    {
        return $this->min_balance;
    }

    public function setMinBalance(int $min_balance): self
    {
        $this->min_balance = $min_balance;

        return $this;
    }


    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|Transfers[]
     */
    public function getTransfers(): Collection
    {
        return $this->transfers;
    }

    public function addTransfer(Transfers $transfer): self
    {
        if (!$this->transfers->contains($transfer)) {
            $this->transfers[] = $transfer;
            $transfer->setAccount($this);
        }

        return $this;
    }

    public function removeTransfer(Transfers $transfer): self
    {
        if ($this->transfers->contains($transfer)) {
            $this->transfers->removeElement($transfer);
            // set the owning side to null (unless already changed)
            if ($transfer->getAccount() === $this) {
                $transfer->setAccount(null);
            }
        }

        return $this;
    }

}
