<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgentsRepository")
 */
class Agents
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=90)
     */
    private $agent_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Accounts", mappedBy="agent")
     */
    private $accounts;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgentName(): ?string
    {
        return $this->agent_name;
    }

    public function setAgentName(string $agent_name): self
    {
        $this->agent_name = $agent_name;

        return $this;
    }

    /**
     * @return Collection|Accounts[]
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    public function addAccount(Accounts $account): self
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts[] = $account;
            $account->setAgent($this);
        }

        return $this;
    }

    public function removeAccount(Accounts $account): self
    {
        if ($this->accounts->contains($account)) {
            $this->accounts->removeElement($account);
            // set the owning side to null (unless already changed)
            if ($account->getAgent() === $this) {
                $account->setAgent(null);
            }
        }

        return $this;
    }
}
