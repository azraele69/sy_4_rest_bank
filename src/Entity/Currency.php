<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $currency_name;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $currency_abbreviation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Accounts", mappedBy="currency")
     */
    private $accounts;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrencyName(): ?string
    {
        return $this->currency_name;
    }

    public function setCurrencyName(string $currency_name): self
    {
        $this->currency_name = $currency_name;

        return $this;
    }

    public function getCurrencyAbbreviation(): ?string
    {
        return $this->currency_abbreviation;
    }

    public function setCurrencyAbbreviation(string $currency_abbreviation): self
    {
        $this->currency_abbreviation = $currency_abbreviation;

        return $this;
    }

    /**
     * @return Collection|Accounts[]
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    public function addAccount(Accounts $account): self
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts[] = $account;
            $account->setCurrency($this);
        }

        return $this;
    }

    public function removeAccount(Accounts $account): self
    {
        if ($this->accounts->contains($account)) {
            $this->accounts->removeElement($account);
            // set the owning side to null (unless already changed)
            if ($account->getCurrency() === $this) {
                $account->setCurrency(null);
            }
        }

        return $this;
    }
}
