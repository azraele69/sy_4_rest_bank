<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransfersRepository")
 */
class Transfers
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="date", nullable=true)
   */
  private $date;

  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\Accounts", inversedBy="transfers")
   */
  private $account;

  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\TransferType", inversedBy="transfers")
   */
  private $type;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $reference;

  /**
   * @ORM\Column(type="float", nullable=true)
   */
  private $debit;

  /**
   * @ORM\Column(type="float", nullable=true)
   */
  private $credit;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $balance;

  /**
   * @ORM\Column(type="string", length=25, nullable=true)
   */
  private $intra_to_account_number;

  /**
   * @ORM\Column(type="string", length=100, nullable=true)
   */
  private $bank_name;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $bank_address;

  /**
   * @ORM\Column(type="string", length=20, nullable=true)
   */
  private $swift_code;

  /**
   * @ORM\Column(type="string", length=50, nullable=true)
   */
  private $iban_code;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $beneficiary_country;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $beneficiary_city;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $beneficiary_address;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $pay_charges;

  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\TransferStatus", inversedBy="transfers")
   */
  private $status;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $manager_reference;

  /**
   * @ORM\Column(type="string", length=20, nullable=true)
   */
  private $transfer_number;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getDate(): ?\DateTimeInterface
  {
    return $this->date;
  }

  public function setDate(\DateTimeInterface $date): self
  {
    $this->date = $date;

    return $this;
  }

  public function getAccount(): ?Accounts
  {
    return $this->account;
  }

  public function setAccount(?Accounts $account): self
  {
    $this->account = $account;

    return $this;
  }

  public function getReference(): ?string
  {
    return $this->reference;
  }

  public function setReference(string $reference): self
  {
    $this->reference = $reference;

    return $this;
  }

  public function getDebit(): ?float
  {
    return $this->debit;
  }

  public function setDebit(float $debit): self
  {
    $this->debit = $debit;

    return $this;
  }

  public function getCredit(): ?float
  {
    return $this->credit;
  }

  public function setCredit(?float $credit): self
  {
    $this->credit = $credit;

    return $this;
  }

  public function getBalance(): ?string
  {
    return $this->balance;
  }

  public function setBalance(string $balance): self
  {
    $this->balance = $balance;

    return $this;
  }

  public function getIntraToAccountNumber(): ?string
  {
    return $this->intra_to_account_number;
  }

  public function setIntraToAccountNumber(?string $intra_to_account_number): self
  {
    $this->intra_to_account_number = $intra_to_account_number;

    return $this;
  }

  public function getBankName(): ?string
  {
    return $this->bank_name;
  }

  public function setBankName(?string $bank_name): self
  {
    $this->bank_name = $bank_name;

    return $this;
  }

  public function getBankAddress(): ?string
  {
    return $this->bank_address;
  }

  public function setBankAddress(?string $bank_address): self
  {
    $this->bank_address = $bank_address;

    return $this;
  }

  public function getSwiftCode(): ?string
  {
    return $this->swift_code;
  }

  public function setSwiftCode(?string $swift_code): self
  {
    $this->swift_code = $swift_code;

    return $this;
  }

  public function getIbanCode(): ?string
  {
    return $this->iban_code;
  }

  public function setIbanCode(?string $iban_code): self
  {
    $this->iban_code = $iban_code;

    return $this;
  }

  public function getBeneficiaryCountry(): ?string
  {
    return $this->beneficiary_country;
  }

  public function setBeneficiaryCountry(?string $beneficiary_country): self
  {
    $this->beneficiary_country = $beneficiary_country;

    return $this;
  }

  public function getBeneficiaryCity(): ?string
  {
    return $this->beneficiary_city;
  }

  public function setBeneficiaryCity(?string $beneficiary_city): self
  {
    $this->beneficiary_city = $beneficiary_city;

    return $this;
  }

  public function getBeneficiaryAddress(): ?string
  {
    return $this->beneficiary_address;
  }

  public function setBeneficiaryAddress(?string $beneficiary_address): self
  {
    $this->beneficiary_address = $beneficiary_address;

    return $this;
  }

  public function getPayCharges(): ?string
  {
    return $this->pay_charges;
  }

  public function setPayCharges(?string $pay_charges): self
  {
    $this->pay_charges = $pay_charges;

    return $this;
  }

  public function getType(): ?TransferType
  {
    return $this->type;
  }

  public function setType(?TransferType $type): self
  {
    $this->type = $type;

    return $this;
  }

  public function getStatus(): ?TransferStatus
  {
    return $this->status;
  }

  public function setStatus(?TransferStatus $status): self
  {
    $this->status = $status;

    return $this;
  }

  public function getManagerReference(): ?string
  {
    return $this->manager_reference;
  }

  public function setManagerReference(?string $manager_reference): self
  {
    $this->manager_reference = $manager_reference;

    return $this;
  }

  public function getTransferNumber(): ?string
  {
      return $this->transfer_number;
  }

  public function setTransferNumber(?string $transfer_number): self
  {
      $this->transfer_number = $transfer_number;

      return $this;
  }
}
