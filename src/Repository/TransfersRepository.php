<?php

namespace App\Repository;

use App\Entity\Transfers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Transfers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transfers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transfers[]    findAll()
 * @method Transfers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransfersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transfers::class);
    }

  public function getIntraTransferList()
  {
    return $this->createQueryBuilder('t')
      ->select('t.id')
      ->addSelect('t.date')
      ->addSelect('t.transfer_number')
      ->innerJoin('t.status', 'status')
      ->addSelect('status.status_name')
      ->innerJoin('t.type', 'type')
      ->where('type.id = :type_id')
      ->setParameter('type_id', '1')
      ->innerJoin('t.account', 'account')
      ->addSelect('account.balance')
      ->addSelect('account.min_balance')
      ->addSelect('t.reference')
      ->innerJoin('account.user', 'user')
      ->addSelect('user.client_id')
      ->addSelect('user.login')
      ->addSelect('user.first_name')
      ->addSelect('user.last_name')
      ->addSelect('account.account_special_number')
      ->innerJoin('account.currency', 'currency')
      ->addSelect('currency.currency_abbreviation')
      ->addSelect('t.intra_to_account_number')
      ->addSelect('t.debit as amount')

      ->getQuery()
      ->execute(array(), Query::HYDRATE_ARRAY);
  }

  public function getPaymentRequestTransferList()
  {
    return $this->createQueryBuilder('t')
      ->select('t.id')
      ->addSelect('t.date')
      ->innerJoin('t.account', 'account')
      ->innerJoin('account.user', 'user')
      ->innerJoin('t.type', 'type')
      ->where('type.id = :tid')
      ->setParameter('tid', '2')
      ->addSelect('user.client_id')
      ->addSelect('user.first_name')
      ->addSelect('user.last_name')
      ->addSelect('user.login')
      ->addSelect('account.account_special_number')
      ->addSelect('account.balance')
      ->addSelect('account.min_balance')
      ->innerJoin('account.currency', 'currency')
      ->addSelect('currency.currency_abbreviation')
      ->addSelect('t.transfer_number')
      ->addSelect('t.bank_name')
      ->addSelect('t.bank_address')
      ->addSelect('t.swift_code')
      ->addSelect('t.iban_code')
      ->addSelect('t.beneficiary_country')
      ->addSelect('t.beneficiary_city')
      ->addSelect('t.beneficiary_address')
      ->addSelect('t.reference')
      ->addSelect('t.pay_charges')
      ->addSelect('t.debit as amount')
      ->innerJoin('t.status', 'status')
      ->addSelect('status.status_name')



      ->getQuery()
      ->execute(array(), Query::HYDRATE_ARRAY);
  }

  public function getTransactionByAccount($account)
  {
    return $this->createQueryBuilder('t')
      ->select('t.id')
      ->addSelect('t.date')
      ->innerJoin('t.account', 'account')
      ->innerJoin('t.type', 'type')

      ->addSelect('type.transfer_type_name')
      ->where('account.account_special_number = :account')
      ->setParameter('account', $account)
      ->addSelect('account.account_special_number')
      ->addSelect('t.balance')

      ->addSelect('account.min_balance')
      ->innerJoin('account.currency', 'currency')
      ->addSelect('currency.currency_abbreviation')
      ->addSelect('t.transfer_number')
      ->addSelect('t.bank_name')
      ->addSelect('t.bank_address')
      ->addSelect('t.intra_to_account_number')
      ->addSelect('t.swift_code')
      ->addSelect('t.iban_code')
      ->addSelect('t.beneficiary_country')
      ->addSelect('t.beneficiary_city')
      ->addSelect('t.beneficiary_address')
      ->addSelect('t.reference')
      ->addSelect('t.pay_charges')
      ->addSelect('t.debit')
      ->addSelect('t.credit')
      ->innerJoin('t.status', 'status')
      ->addSelect('status.status_name')

      ->orderBy('t.date', 'DESC' )
      ->addOrderBy('t.id', 'DESC')

      ->getQuery()
      ->execute(array(), Query::HYDRATE_ARRAY);
  }

    // /**
    //  * @return Transfers[] Returns an array of Transfers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transfers
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
