<?php

namespace App\Repository;

use App\Entity\Accounts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Accounts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Accounts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Accounts[]    findAll()
 * @method Accounts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountsRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Accounts::class);
  }
  public function getAccountsByNumber($number)
  {
    return $this->createQueryBuilder('a')
      ->select('a.id')
      ->addSelect('a.account_special_number')
      ->innerJoin('a.account_type', 'at')
      ->addSelect('at.account_type_name')
      ->innerJoin('a.currency', 'ac')
      ->addSelect('ac.currency_abbreviation')
      ->addSelect('a.balance')
      ->addSelect('a.min_balance')
      ->addSelect('a.status')
      ->where('a.account_special_number = :number')
      ->setParameter('number', $number)

      ->getQuery()
      ->execute(array(), Query::HYDRATE_ARRAY);
  }

  public function getAccountsByUser($user)
  {
    return $this->createQueryBuilder('a')
      ->select('a.id')
      ->addSelect('a.account_special_number')
      ->innerJoin('a.account_type', 'at')
      ->addSelect('at.account_type_name')
      ->innerJoin('a.currency', 'ac')
      ->addSelect('ac.currency_abbreviation')
      ->addSelect('a.balance')
      ->addSelect('a.min_balance')
      ->addSelect('a.status')
      ->where('a.user = :user')
      ->setParameter('user', $user)

      ->getQuery()
      ->execute(array(), Query::HYDRATE_ARRAY);
  }

  public function getAccountsList()
  {
    return $this->createQueryBuilder('a')
      ->select('a.id')
      ->addSelect('a.account_special_number')
      ->innerJoin('a.account_type', 'at')
      ->addSelect('at.account_type_name')
      ->innerJoin('a.currency', 'ac')
      ->addSelect('ac.currency_abbreviation')
      ->addSelect('a.balance')
      ->addSelect('a.min_balance')
      ->addSelect('a.status')
      ->innerJoin('a.user', 'user')
      ->addSelect('user.login')
      ->addSelect('user.client_id')
      ->addSelect('user.first_name')
      ->addSelect('user.last_name')
      ->orderBy('user.login', 'ASC')

      ->getQuery()
      ->execute(array(), Query::HYDRATE_ARRAY);
  }


  // /**
  //  * @return Accounts[] Returns an array of Accounts objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('a')
          ->andWhere('a.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('a.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?Accounts
  {
      return $this->createQueryBuilder('a')
          ->andWhere('a.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
