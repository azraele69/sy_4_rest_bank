<?php

namespace App\Repository;

use App\Entity\TransferType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TransferType|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransferType|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransferType[]    findAll()
 * @method TransferType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransferTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransferType::class);
    }

    // /**
    //  * @return TransferType[] Returns an array of TransferType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TransferType
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
