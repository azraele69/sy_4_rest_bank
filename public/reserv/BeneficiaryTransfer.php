<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BeneficiaryTransferRepository")
 */
class BeneficiaryTransfer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Accounts", inversedBy="beneficiaryTransfers")
     */
    private $account;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bank_address;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $swift_code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $iban_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $beneficiary_country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $beneficiary_city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $beneficiary_address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $beneficiary_reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pay_charges;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $transfer_number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TransferStatus", inversedBy="beneficiaryTransfers")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bank_name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccount(): ?Accounts
    {
        return $this->account;
    }

    public function setAccount(?Accounts $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getBankAddress(): ?string
    {
        return $this->bank_address;
    }

    public function setBankAddress(string $bank_address): self
    {
        $this->bank_address = $bank_address;

        return $this;
    }

    public function getSwiftCode(): ?string
    {
        return $this->swift_code;
    }

    public function setSwiftCode(string $swift_code): self
    {
        $this->swift_code = $swift_code;

        return $this;
    }

    public function getIbanCode(): ?string
    {
        return $this->iban_code;
    }

    public function setIbanCode(string $iban_code): self
    {
        $this->iban_code = $iban_code;

        return $this;
    }

    public function getBeneficiaryCountry(): ?string
    {
        return $this->beneficiary_country;
    }

    public function setBeneficiaryCountry(string $beneficiary_country): self
    {
        $this->beneficiary_country = $beneficiary_country;

        return $this;
    }

    public function getBeneficiaryCity(): ?string
    {
        return $this->beneficiary_city;
    }

    public function setBeneficiaryCity(string $beneficiary_city): self
    {
        $this->beneficiary_city = $beneficiary_city;

        return $this;
    }

    public function getBeneficiaryAddress(): ?string
    {
        return $this->beneficiary_address;
    }

    public function setBeneficiaryAddress(string $beneficiary_address): self
    {
        $this->beneficiary_address = $beneficiary_address;

        return $this;
    }

    public function getBeneficiaryReference(): ?string
    {
        return $this->beneficiary_reference;
    }

    public function setBeneficiaryReference(string $beneficiary_reference): self
    {
        $this->beneficiary_reference = $beneficiary_reference;

        return $this;
    }

    public function getPayCharges(): ?string
    {
        return $this->pay_charges;
    }

    public function setPayCharges(string $pay_charges): self
    {
        $this->pay_charges = $pay_charges;

        return $this;
    }

    public function getTransferNumber(): ?string
    {
        return $this->transfer_number;
    }

    public function setTransferNumber(string $transfer_number): self
    {
        $this->transfer_number = $transfer_number;

        return $this;
    }

    public function getStatus(): ?TransferStatus
    {
        return $this->status;
    }

    public function setStatus(?TransferStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bank_name;
    }

    public function setBankName(string $bank_name): self
    {
        $this->bank_name = $bank_name;

        return $this;
    }

}
