<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IntraTransfersRepository")
 */
class IntraTransfers
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Accounts", inversedBy="from_accounts")
     */
    private $from_accounts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Accounts", inversedBy="to_account")
     */
    private $to_accounts;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TransferStatus", inversedBy="intraTransfers")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $transfer_number;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFromAccounts(): ?Accounts
    {
        return $this->from_accounts;
    }

    public function setFromAccounts(?Accounts $from_accounts): self
    {
        $this->from_accounts = $from_accounts;

        return $this;
    }

    public function getToAccounts(): ?Accounts
    {
        return $this->to_accounts;
    }

    public function setToAccounts(?Accounts $to_accounts): self
    {
        $this->to_accounts = $to_accounts;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getStatus(): ?TransferStatus
    {
        return $this->status;
    }

    public function setStatus(?TransferStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTransferNumber(): ?string
    {
        return $this->transfer_number;
    }

    public function setTransferNumber(?string $transfer_number): self
    {
        $this->transfer_number = $transfer_number;

        return $this;
    }
}
