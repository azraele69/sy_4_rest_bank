<?php

namespace App\Controller;

use App\Entity\Accounts;
use App\Entity\BeneficiaryTransfer;
use App\Entity\IntraTransfers;
use App\Entity\TransferStatus;
use App\Entity\User;
use App\Service\KeyGeneration;
use App\Service\MailerService;
use App\Service\UserDecodeService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TransfersControllerDump extends AbstractFOSRestController
{

  private $role_name = 'CLIENT';
  private $uds;
  private $em;
  private $mailerService;

  public function __construct(UserDecodeService $decodeService, EntityManagerInterface $entityManager, MailerService $mailerService)
  {
    $this->em = $entityManager;
    $this->uds = $decodeService;
    $this->mailerService = $mailerService;
  }

  public function postApplicationAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      // Проровряе принадлижат ли счета одному пользователю
      if (!$this->accountReconciliation($request)) {
        return $this->view(array('message' => 'Accounts must belong to one user'), Response::HTTP_FORBIDDEN);
      }
      // Проверяем привышены ли лимиты на стнятие
      if ($this->WithdrawalLimit($request)) {
        return $this->view(array('message' => 'Withdrawal limit exceeded'), Response::HTTP_FORBIDDEN);
      }

      $transfer = new IntraTransfers();
      $transfer->setFromAccounts($this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]));
      $transfer->setToAccounts($this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('to_account')]));
      $transfer->setAmount($request->get('amount'));
      $transfer->setReference($request->get('reference'));
      $transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(1));
      $transfer->setTransferNumber(KeyGeneration::createTransferNumber());

      $this->em->persist($transfer);
      $this->em->flush();

      $this->mailerService->sendApplicationToTransaction('Intra-Transfers');

      return $this->view($transfer->getId(), Response::HTTP_OK);

    } else {
      return $this->view(array('message' => 'Access closed'), Response::HTTP_FORBIDDEN);
    }
  }

  public function postApplicationBeneficiaryTransferAction(Request $request)
  {
    if ($this->uds->CheckRole($request, $this->role_name)) {
      // Проровряем принадлижит ли счет пользователю
      if (!$this->masterOfAccounts($request)) {
        return $this->view(array('message' => 'User is not an account owner'), Response::HTTP_CONFLICT);
      }
      // Проверяем привышены ли лимиты на стнятие
      if ($this->WithdrawalLimit($request)) {
        return $this->view(array('message' => 'Withdrawal limit exceeded'), Response::HTTP_CONFLICT);
      }

      $beneficiaries_transfer = new BeneficiaryTransfer();
      $beneficiaries_transfer->setAccount($this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]));
      $beneficiaries_transfer->setAmount($request->get('Amount'));
      $beneficiaries_transfer->setBankAddress($request->get('Bank Address'));
      $beneficiaries_transfer->setSwiftCode($request->get('SWIFT Code'));
      $beneficiaries_transfer->setBankName($request->get('Bank Name'));
      $beneficiaries_transfer->setIbanCode($request->get('IBAN/Beneficiary Account Number'));
      $beneficiaries_transfer->setBeneficiaryCountry($request->get('Beneficiary Country'));
      $beneficiaries_transfer->setBeneficiaryCity($request->get('Beneficiary City'));
      $beneficiaries_transfer->setBeneficiaryAddress($request->get('Beneficiary Address'));
      $beneficiaries_transfer->setBeneficiaryReference($request->get('Beneficiary Reference'));
      $beneficiaries_transfer->setPayCharges($request->get('PayCharges'));
      $beneficiaries_transfer->setTransferNumber(KeyGeneration::createTransferNumber());
      $beneficiaries_transfer->setStatus($this->getDoctrine()->getRepository(TransferStatus::class)->find(1));

      $this->em->persist($beneficiaries_transfer);
      $this->em->flush();

      $this->mailerService->sendApplicationToTransaction('Payment Request');

      return $this->view($beneficiaries_transfer->getId(), Response::HTTP_OK);
    } else {
      return $this->view(array('message' => 'Access closed'), Response::HTTP_FORBIDDEN);
    }
  }

  private function masterOfAccounts(Request $request)
  {
    //Получаем пользователя сделавшего запрос на транзакцию
    $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('login' => $this->uds->getUserNameByToken($request)));
    $account = $this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]);
    // получаем владельца счёта
    $account_master = $account->getUser();

    // Проверяем совпадет ли владелец и заявитель
    return $user->getId() == $account_master->getId()? true : false;
  }

  private function accountReconciliation(Request $request)
  {
    $from = $this->getDoctrine()->getRepository(User::class)->getUserByAccount($request->get('from_account'));
    $to = $this->getDoctrine()->getRepository(User::class)->getUserByAccount($request->get('to_account'));
    return ($from[0]['id'] === $to[0]['id']) ? true : false;
  }

  private function WithdrawalLimit(Request $request)
  {
    $from = $this->getDoctrine()->getRepository(Accounts::class)->findOneBy(['account_special_number' => $request->get('from_account')]);
    return ($from->getBalance() - $from->getMinBalance()) < $request->get('amount') ? true : false;
  }

}
